package main

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"net/http"
)

func infoHandler(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "GET" {
		return
	}
	user := r.Context().Value("user")
	claims := user.(*jwt.Token).Claims.(jwt.MapClaims)
	username := claims["user"].(string)
	id := claims["id"]
	output, _ := json.Marshal(map[string]interface{}{"status": "ok", "user": username, "id": id})

	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(output)
}
