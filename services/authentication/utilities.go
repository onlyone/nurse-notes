package main

import (
	"database/sql"
	"golang.org/x/crypto/bcrypt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

const (
	dbDriver = "mysql"
	dbUser   = "db_user"
	dbPass   = "db_pwd"
	dbName   = "users"
	dbHost   = "users_db_master:3306"
)

func dbConn() (db *sql.DB) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+dbHost+")/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func comparePasswords(encrypted, attempt string) error {
	return bcrypt.CompareHashAndPassword([]byte(encrypted), []byte(attempt))
}

func allowCors(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}
