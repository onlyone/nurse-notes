package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

const (
	ServerAddr = ":8080"
	ApiRoot    = "/api/v1"
)

var (
	AppKey string
)

func init() {
	if err := initAppKey(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	http.HandleFunc(ApiRoot+"/token", tokenHandler)
	http.Handle(ApiRoot+"/info", authMiddleware(http.HandlerFunc(infoHandler)))

	log.Println("Authentication Service started at", ServerAddr)
	log.Fatal(http.ListenAndServe(ServerAddr, nil))
}

func initAppKey() error {
	keyFile, err := os.Open(".app-key")
	if err != nil {
		return fmt.Errorf(`please provide an app key in a ".app-key" file`)
	}
	keyBytes, err := ioutil.ReadAll(keyFile)
	if err != nil {
		return fmt.Errorf(`error when reading the ".app-key" file`)
	}
	if len(keyBytes) < 10 {
		return fmt.Errorf(`the length of the key in the ".app-key" file should be no less than 10 bytes`)
	}
	AppKey = string(keyBytes)
	return nil
}
