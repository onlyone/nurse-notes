package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestInfoHandler(t *testing.T) {
	// Setup
	const username = "test-user"
	const id = 22
	token, err := generateToken(username, id)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", "/api/v1/info", nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization", "Bearer "+token)

	rr := httptest.NewRecorder()
	handler := authMiddleware(http.HandlerFunc(infoHandler))

	// Execute
	handler.ServeHTTP(rr, req)

	// Check the status code
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// Check the response body
	var res map[string]interface{}

	if err := json.Unmarshal(rr.Body.Bytes(), &res); err != nil {
		t.Fatalf("response body cannot be parsed: %v", rr.Body.String())
	}

	expected, ok := res["user"]
	if !ok || expected != username {
		t.Fatalf("correct username cannot be retrived")
	}

	expectedId := int64(res["id"].(float64))
	if expectedId != id {
		t.Fatalf("correct id cannot be retrived")
	}
}
