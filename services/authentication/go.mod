module nurse-notes/authenication

go 1.14

require (
	github.com/auth0/go-jwt-middleware v0.0.0-20200810150920-a32d7af194d1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
)
