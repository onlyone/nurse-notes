package main

import (
	"encoding/json"
	"fmt"
	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"io"
	"log"
	"net/http"
	"time"
)

const (
	TokenLife = time.Hour * 6
)

type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (c *Credentials) Retrieve(username string) (uid int64, err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	query, err := db.Query("SELECT id,username,password FROM users WHERE username=?", username)
	if err != nil {
		return
	}
	if !query.Next() {
		return 0, fmt.Errorf("there is no such user")
	}
	err = query.Scan(&uid, &c.Username, &c.Password)
	return
}

func tokenHandler(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	w.Header().Add("Content-Type", "application/json")

	var attemptCred, dbCred Credentials
	if err := json.NewDecoder(r.Body).Decode(&attemptCred); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, _ = io.WriteString(w, `{"error":"invalid request"}`)
		return
	}

	uid, err := dbCred.Retrieve(attemptCred.Username)
	if err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		_, _ = io.WriteString(w, `{"error":"invalid_credentials"}`)
		return
	}

	if err := comparePasswords(dbCred.Password, attemptCred.Password); err != nil {
		w.WriteHeader(http.StatusUnauthorized)
		_, _ = io.WriteString(w, `{"error":"invalid_credentials"}`)
		return
	}

	token, err := generateToken(attemptCred.Username, uid)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = io.WriteString(w, `{"error":"token_generation_failed"}`)
		return
	}
	_, _ = io.WriteString(w, `{"token":"`+token+`"}`)
	return
}

func generateToken(username string, uid int64) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": username,
		"id":   uid,
		"exp":  time.Now().Add(TokenLife).Unix(),
		"iat":  time.Now().Unix(),
	})
	return token.SignedString([]byte(AppKey))
}

func authMiddleware(next http.Handler) http.Handler {
	if len(AppKey) == 0 {
		log.Fatal("HTTP server unable to start, expected an AppKey for JWT auth")
	}
	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(AppKey), nil
		},
		SigningMethod: jwt.SigningMethodHS256,
	})
	return jwtMiddleware.Handler(next)
}
