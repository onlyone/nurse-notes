package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
)

const (
	ServerAddr = ":8081"
	ApiRoot    = "/api/v1/users"
)

func init() {
	db := dbConn()
	for {
		err := db.Ping()
		if err == nil {
			log.Println("Connected to DB.")
			break
		}
		log.Println(err, "Waiting for DB connection...")
		time.Sleep(time.Second * 2)
	}
	defer func() { _ = db.Close() }()

	exist, err := doesUsersTableExist(db)
	if err != nil {
		log.Fatal(err)
	}
	if exist {
		return
	}

	log.Println("Users table does not exist. Creating...")
	if err = createUsersTable(db); err != nil {
		log.Fatal(err)
	}
	if err = seedUsersTable(db); err != nil {
		log.Fatal(err)
	}
}

func main() {
	http.HandleFunc(ApiRoot+"/", Index)
	http.HandleFunc(ApiRoot+"/show", Show)
	http.HandleFunc(ApiRoot+"/insert", Insert)
	http.HandleFunc(ApiRoot+"/update", Update)
	http.HandleFunc(ApiRoot+"/delete", Delete)

	log.Println("User Service started at", ServerAddr)
	log.Fatal(http.ListenAndServe(ServerAddr, nil))
}

func Index(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "GET" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	all, err := AllUsers()
	if err != nil {
		ResponseError(w, err)
		return
	}
	data, _ := json.Marshal(all)
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(data)
}

func Show(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "GET" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	var user User
	if err := user.Retrieve(r.URL.Query().Get("id")); err != nil {
		ResponseError(w, err)
		return
	}
	data, _ := json.Marshal(user)
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(data)
}

func Insert(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		ResponseError(w, err)
		return
	}
	if err := user.Insert(); err != nil {
		ResponseError(w, err)
		return
	}

	data, _ := json.Marshal(user)
	_, _ = w.Write(data)
}

func Update(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		ResponseError(w, err)
		return
	}
	if err := user.Update(); err != nil {
		ResponseError(w, err)
		return
	}
	log.Printf("UPDATE: Username: %s | Password: %s\n", user.Username, user.Password)
	Show(w, r)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		ResponseError(w, err)
		return
	}
	if err := user.Delete(); err != nil {
		ResponseError(w, err)
		return
	}
	_, _ = io.WriteString(w, `{"status":"success"}`)
}
