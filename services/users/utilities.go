package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/bcrypt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

const (
	dbDriver = "mysql"
	dbUser   = "db_user"
	dbPass   = "db_pwd"
	dbName   = "users"
	dbHost   = "users_db_master:3306"
)

func dbConn() (db *sql.DB) {
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+dbHost+")/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func ResponseError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	_, _ = io.WriteString(w, `{"error":"`+err.Error()+`"}`)
}

func encryptPassword(pwd string) ([]byte, error) {
	if len(pwd) < 8 {
		return nil, fmt.Errorf("the length of password needs be at least 8")
	}
	hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	return hash, nil
}

func allowCors(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func getAuth(r *http.Request) error {
	client := &http.Client{Timeout: time.Second * 5}
	token := r.Header.Get("Authorization")
	request, err := http.NewRequest("GET", "http://authentication:8080/api/v1/info", nil)
	if err != nil {
		return err
	}
	if len(token) < 7 {
		return fmt.Errorf("invalid token received")
	}
	request.Header.Set("Authorization", token)
	resp, err := client.Do(request)
	if err != nil {
		return err
	}
	defer func() { _ = resp.Body.Close() }()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	bodyJson := map[string]interface{}{}
	if err = json.Unmarshal(body, &bodyJson); err != nil {
		return err
	}
	status, ok := bodyJson["status"]
	if !ok || status != "ok" {
		return fmt.Errorf("request unauthorized")
	}
	return nil
}
