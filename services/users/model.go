package main

import (
	"fmt"
	"log"
	"strconv"
)

type User struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password,omitempty"`
}

func AllUsers() (all []User, err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	query, err := db.Query("SELECT id,username FROM users ORDER BY id DESC")
	if err != nil {
		return
	}
	all = []User{}
	var user User
	for query.Next() {
		err = query.Scan(&user.Id, &user.Username)
		if err != nil {
			return
		}
		all = append(all, user)
	}
	return
}

func (u *User) Insert() (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()
	stmt, err := db.Prepare("INSERT INTO users(username, password) VALUES(?,?)")
	if err != nil {
		return
	}
	encrypted, err := encryptPassword(u.Password)
	if err != nil {
		return
	}
	result, err := stmt.Exec(u.Username, encrypted)
	if err != nil {
		return
	}
	log.Printf("INSERT: Username: %s | Password: %s\n", u.Username, u.Password)
	id, _ := result.LastInsertId()
	return u.Retrieve(strconv.FormatInt(id, 10))
}

func (u *User) Update() (err error) {
	var updateUsername bool
	if u.Username != "" {
		updateUsername = true
	}

	var updatePassword bool
	var encrypted []byte
	if u.Password != "" {
		updatePassword = true
		encrypted, err = encryptPassword(u.Password)
		if err != nil {
			return
		}
	}

	if !updateUsername && !updatePassword {
		return fmt.Errorf("there is nothing to update for the user")
	}

	db := dbConn()
	defer func() { _ = db.Close() }()
	switch {
	case !updatePassword && updateUsername:
		_, err = db.Exec("UPDATE users SET username=? WHERE id=?", u.Username, u.Id)
	case updatePassword && !updateUsername:
		_, err = db.Exec("UPDATE users SET password=? WHERE id=?", encrypted, u.Id)
	default:
		_, err = db.Exec("UPDATE users SET username=?, password=? WHERE id=?", u.Username, encrypted, u.Id)
	}
	return
}

func (u *User) Retrieve(id string) (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	query, err := db.Query("SELECT id,username FROM users WHERE id=?", id)
	if err != nil {
		return err
	}
	if !query.Next() {
		return fmt.Errorf("there is no such user")
	}
	err = query.Scan(&u.Id, &u.Username)
	return
}

func (u *User) Delete() (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	result, err := db.Exec("DELETE FROM users WHERE id=?", u.Id)
	if err != nil {
		return
	}

	if n, err := result.RowsAffected(); n == 0 || err != nil {
		return fmt.Errorf("no record deleted")
	}
	log.Printf("DELETE: ID: %d\n", u.Id)
	return
}
