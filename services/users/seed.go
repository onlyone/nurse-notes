package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func doesUsersTableExist(db *sql.DB) (bool, error) {
	stmt, err := db.Prepare(`SELECT *
		FROM information_schema.tables
		WHERE table_schema = ?
		AND table_name = 'users'
		LIMIT 1;
	`)
	if err != nil {
		return false, err
	}
	result, err := stmt.Query(dbName)
	if err != nil {
		return false, err
	}
	return result.Next(), nil
}

func createUsersTable(db *sql.DB) error {
	_, err := db.Exec(`
		CREATE TABLE users (
		  id int(6) unsigned NOT NULL AUTO_INCREMENT,
		  username varchar(30) NOT NULL UNIQUE,
		  password varchar(120) NOT NULL,
		  PRIMARY KEY (id)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
	`)
	return err
}

func seedUsersTable(db *sql.DB) error {
	stmt, err := db.Prepare("INSERT INTO users(username, password) VALUES(?,?)")
	if err != nil {
		return err
	}
	usersData := [][]string{
		{"jack", "123456789"},
		{"mary", "33yu1234"},
	}
	for _, row := range usersData {
		encrypted, _ := encryptPassword(row[1])
		if _, err = stmt.Exec(row[0], encrypted); err != nil {
			return err
		}
	}
	return nil
}
