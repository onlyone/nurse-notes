package main

import (
	"fmt"
	"log"
	"strconv"
)

type Note struct {
	Id        int    `json:"id"`
	UserId    int    `json:"user_id"`
	PatientId int    `json:"patient_id"`
	Text      string `json:"text"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func AllNotes(uid int64) (all []Note, err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	query, err := db.Query("SELECT * FROM notes WHERE user_id=? ORDER BY id DESC", uid)
	if err != nil {
		return
	}

	all = []Note{}
	var note Note
	for query.Next() {
		err = query.Scan(&note.Id, &note.UserId, &note.PatientId, &note.Text, &note.CreatedAt, &note.UpdatedAt)
		if err != nil {
			return
		}
		all = append(all, note)
	}
	return
}

func (u *Note) Insert(uid int64) (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	result, err := db.Exec("INSERT INTO notes(user_id,patient_id,text) VALUES(?,?,?)", uid, u.PatientId, u.Text)
	if err != nil {
		return
	}
	log.Printf("INSERT: Text: %s by user %d for patient %d\n", u.Text, uid, u.PatientId)

	id, _ := result.LastInsertId()
	return u.Retrieve(strconv.FormatInt(id, 10), uid)
}

func (u *Note) Update(uid int64) (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	result, err := db.Exec("UPDATE notes SET text=?, patient_id=? WHERE id=? AND user_id=?", u.Text, u.PatientId, u.Id, uid)
	if err != nil {
		return err
	}
	if n, err := result.RowsAffected(); n == 0 || err != nil {
		return fmt.Errorf("no record updated")
	}
	log.Printf("UPDATE: Text: %s for note %d\n", u.Text, u.Id)
	return u.Retrieve(strconv.FormatInt(int64(u.Id), 10), uid)
}

func (u *Note) Retrieve(id string, uid int64) (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	query, err := db.Query("SELECT * FROM notes WHERE id=? AND user_id=?", id, uid)
	if err != nil {
		return err
	}
	if !query.Next() {
		return fmt.Errorf("there is no such note")
	}
	err = query.Scan(&u.Id, &u.UserId, &u.PatientId, &u.Text, &u.CreatedAt, &u.UpdatedAt)
	return
}

func (u *Note) Delete(uid int64) (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	result, err := db.Exec("DELETE FROM notes WHERE id=? AND user_id=?", u.Id, uid)
	if err != nil {
		return
	}

	if n, err := result.RowsAffected(); n == 0 || err != nil {
		return fmt.Errorf("no record deleted")
	}
	log.Printf("DELETE: ID: %d\n", u.Id)
	return
}
