package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func doesNotesTableExist(db *sql.DB) (bool, error) {
	stmt, err := db.Prepare(`SELECT *
		FROM information_schema.tables
		WHERE table_schema = ?
		AND table_name = 'notes'
		LIMIT 1;
	`)
	if err != nil {
		return false, err
	}
	result, err := stmt.Query(dbName)
	if err != nil {
		return false, err
	}
	return result.Next(), nil
}

func createNotesTable(db *sql.DB) error {
	_, err := db.Exec(`
		CREATE TABLE notes (
		  id int(6) unsigned NOT NULL AUTO_INCREMENT,
		  user_id int(6) unsigned NOT NULL,
		  patient_id int(6) unsigned NOT NULL,
		  text varchar(300) NOT NULL,  
		  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  PRIMARY KEY (id)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
	`)
	return err
}
