package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

const (
	ServerAddr = ":8083"
	ApiRoot    = "/api/v1/notes"
)

func init() {
	db := dbConn()
	for {
		err := db.Ping()
		if err == nil {
			log.Println("Connected to DB.")
			break
		}
		log.Println(err, "Waiting for DB connection...")
		time.Sleep(time.Second * 2)
	}
	defer func() { _ = db.Close() }()

	exist, err := doesNotesTableExist(db)
	if err != nil {
		log.Fatal(err)
	}
	if exist {
		return
	}

	log.Println("Notes table does not exist. Creating...")
	if err = createNotesTable(db); err != nil {
		log.Fatal(err)
	}
}

func main() {
	http.HandleFunc(ApiRoot+"/", Index)
	http.HandleFunc(ApiRoot+"/show", Show)
	http.HandleFunc(ApiRoot+"/insert", Insert)
	http.HandleFunc(ApiRoot+"/update", Update)
	http.HandleFunc(ApiRoot+"/delete", Delete)

	log.Println("Note Service started at", ServerAddr)
	log.Fatal(http.ListenAndServe(ServerAddr, nil))
}

func Index(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "GET" {
		return
	}
	uid, err := getAuth(r)
	if err != nil {
		ResponseError(w, err)
		return
	}

	all, err := AllNotes(uid)
	if err != nil {
		ResponseError(w, err)
		return
	}
	data, _ := json.Marshal(all)
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(data)
}

func Show(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "GET" {
		return
	}
	uid, err := getAuth(r)
	if err != nil {
		ResponseError(w, err)
		return
	}

	var note Note
	if err := note.Retrieve(r.URL.Query().Get("id"), uid); err != nil {
		ResponseError(w, err)
		return
	}
	data, _ := json.Marshal(note)
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(data)
}

func Insert(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	id, err := getAuth(r)
	if err != nil {
		ResponseError(w, err)
		return
	}

	var note Note
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		ResponseError(w, err)
		return
	}
	if err := note.Insert(id); err != nil {
		ResponseError(w, err)
		return
	}

	data, _ := json.Marshal(note)
	_, _ = w.Write(data)
}

func Update(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	id, err := getAuth(r)
	if err != nil {
		ResponseError(w, err)
		return
	}

	var note Note
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		ResponseError(w, err)
		return
	}
	if err := note.Update(id); err != nil {
		ResponseError(w, err)
		return
	}

	data, _ := json.Marshal(note)
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(data)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	id, err := getAuth(r)
	if err != nil {
		ResponseError(w, err)
		return
	}

	var note Note
	if err := json.NewDecoder(r.Body).Decode(&note); err != nil {
		ResponseError(w, err)
		return
	}
	if err := note.Delete(id); err != nil {
		ResponseError(w, err)
		return
	}

	_, _ = io.WriteString(w, `{"status":"success"}`)
}
