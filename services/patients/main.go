package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

const (
	ServerAddr = ":8082"
	ApiRoot    = "/api/v1/patients"
)

func init() {
	db := dbConn()
	for {
		err := db.Ping()
		if err == nil {
			log.Println("Connected to DB.")
			break
		}
		log.Println(err, "Waiting for DB connection...")
		time.Sleep(time.Second * 2)
	}
	defer func() { _ = db.Close() }()

	exist, err := doesPatientsTableExist(db)
	if err != nil {
		log.Fatal(err)
	}
	if exist {
		return
	}

	log.Println("Patients table does not exist. Creating...")
	if err = createPatientsTable(db); err != nil {
		log.Fatal(err)
	}
	if err = seedPatientsTable(db); err != nil {
		log.Fatal(err)
	}
}

func main() {
	http.HandleFunc(ApiRoot+"/", Index)
	http.HandleFunc(ApiRoot+"/show", Show)
	http.HandleFunc(ApiRoot+"/insert", Insert)
	http.HandleFunc(ApiRoot+"/update", Update)
	http.HandleFunc(ApiRoot+"/delete", Delete)

	log.Println("Patient Service started at", ServerAddr)
	log.Fatal(http.ListenAndServe(ServerAddr, nil))
}

func Index(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "GET" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	all, err := AllPatients()
	if err != nil {
		ResponseError(w, err)
		return
	}
	data, _ := json.Marshal(all)
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(data)
}

func Show(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "GET" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	var patient Patient
	if err := patient.Retrieve(r.URL.Query().Get("id")); err != nil {
		ResponseError(w, err)
		return
	}
	data, _ := json.Marshal(patient)
	w.Header().Add("Content-Type", "application/json")
	_, _ = w.Write(data)
}

func Insert(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	var patient Patient
	if err := json.NewDecoder(r.Body).Decode(&patient); err != nil {
		ResponseError(w, err)
		return
	}
	if err := patient.Insert(); err != nil {
		ResponseError(w, err)
		return
	}

	data, _ := json.Marshal(patient)
	_, _ = w.Write(data)
}

func Update(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	var patient Patient
	if err := json.NewDecoder(r.Body).Decode(&patient); err != nil {
		ResponseError(w, err)
		return
	}
	if err := patient.Update(); err != nil {
		ResponseError(w, err)
		return
	}
	Show(w, r)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	allowCors(w, r)
	if r.Method != "POST" {
		return
	}
	if err := getAuth(r); err != nil {
		ResponseError(w, err)
		return
	}

	var patient Patient
	if err := json.NewDecoder(r.Body).Decode(&patient); err != nil {
		ResponseError(w, err)
		return
	}
	if err := patient.Delete(); err != nil {
		ResponseError(w, err)
		return
	}

	_, _ = io.WriteString(w, `{"status":"success"}`)
}
