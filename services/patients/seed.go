package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func doesPatientsTableExist(db *sql.DB) (bool, error) {
	stmt, err := db.Prepare(`SELECT *
		FROM information_schema.tables
		WHERE table_schema = ?
		AND table_name = 'patients'
		LIMIT 1;
	`)
	if err != nil {
		return false, err
	}
	result, err := stmt.Query(dbName)
	if err != nil {
		return false, err
	}
	return result.Next(), nil
}

func createPatientsTable(db *sql.DB) error {
	_, err := db.Exec(`
		CREATE TABLE patients (
		  id int(6) unsigned NOT NULL AUTO_INCREMENT,
		  name varchar(30) NOT NULL,
		  PRIMARY KEY (id)
		) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
	`)
	return err
}

func seedPatientsTable(db *sql.DB) error {
	stmt, err := db.Prepare("INSERT INTO patients(name) VALUES(?)")
	if err != nil {
		return err
	}
	patientsData := [][]string{{"Tatsuya"}, {"Tommy"}, {"Miko"}}
	for _, row := range patientsData {
		if _, err = stmt.Exec(row[0]); err != nil {
			return err
		}
	}
	return nil
}
