package main

import (
	"fmt"
	"log"
	"strconv"
)

type Patient struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

func AllPatients() (all []Patient, err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	query, err := db.Query("SELECT id,name FROM patients ORDER BY id DESC")
	if err != nil {
		return
	}
	all = []Patient{}
	var patient Patient
	for query.Next() {
		err = query.Scan(&patient.Id, &patient.Name)
		if err != nil {
			return
		}
		all = append(all, patient)
	}
	return
}

func (u *Patient) Insert() (err error) {
	if len(u.Name) < 5 {
		return fmt.Errorf("name is too short(<5)")
	}
	db := dbConn()
	defer func() { _ = db.Close() }()

	result, err := db.Exec("INSERT INTO patients(name) VALUES(?)", u.Name)
	if err != nil {
		return
	}
	log.Printf("INSERT: Name: %s\n", u.Name)

	id, _ := result.LastInsertId()
	return u.Retrieve(strconv.FormatInt(id, 10))
}

func (u *Patient) Update() (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	result, err := db.Exec("UPDATE patients SET name=? WHERE id=?", u.Name)
	if err != nil {
		return err
	}
	if n, err := result.RowsAffected(); n == 0 || err != nil {
		return fmt.Errorf("no record updated")
	}
	log.Printf("UPDATE: Name: %s\n", u.Name)
	return
}

func (u *Patient) Retrieve(id string) (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	query, err := db.Query("SELECT id,name FROM patients WHERE id=?", id)
	if err != nil {
		return err
	}
	if !query.Next() {
		return fmt.Errorf("there is no such patient")
	}
	err = query.Scan(&u.Id, &u.Name)
	return
}

func (u *Patient) Delete() (err error) {
	db := dbConn()
	defer func() { _ = db.Close() }()

	result, err := db.Exec("DELETE FROM patients WHERE id=?", u.Id)
	if err != nil {
		return
	}

	if n, err := result.RowsAffected(); n == 0 || err != nil {
		return fmt.Errorf("no record deleted")
	}
	log.Printf("DELETE: ID: %d\n", u.Id)
	return
}
