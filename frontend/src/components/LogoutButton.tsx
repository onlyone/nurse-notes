import React from "react"
import { IonButton } from "@ionic/react"
import auth from "../services/Authentication"
import { useHistory } from "react-router"

const LogoutButton: React.FC = () => {
  const history = useHistory()
  const handleLogout = () => {
    auth.resetToken()
    history.push("/login")
  }

  return <IonButton onClick={handleLogout}>Logout as {auth.username}</IonButton>
}

export default LogoutButton
