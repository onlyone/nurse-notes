import React from "react"
import { Redirect, Route } from "react-router-dom"
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs
} from "@ionic/react"
import { IonReactRouter } from "@ionic/react-router"
import { ellipse, square, triangle } from "ionicons/icons"
import NotesPage from "./pages/NotesPage"
import PatientsPage from "./pages/PatientsPage"
import NursesPage from "./pages/NursesPage"
import Login from "./pages/Login"

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css"

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css"
import "@ionic/react/css/structure.css"
import "@ionic/react/css/typography.css"

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css"
import "@ionic/react/css/float-elements.css"
import "@ionic/react/css/text-alignment.css"
import "@ionic/react/css/text-transformation.css"
import "@ionic/react/css/flex-utils.css"
import "@ionic/react/css/display.css"

/* Theme variables */
import "./theme/variables.css"

import { createBrowserHistory } from "history"
import auth from "./services/Authentication"

const App: React.FC = () => {
  const history = createBrowserHistory()

  React.useEffect(() => {
    const checkRoute = (loc: any) => {
      if (auth.isAuthenticated && loc.pathname.startsWith("/login")) {
        setTimeout(()=>history.push("/users/notes"), 10)
      } else if (!auth.isAuthenticated && !loc.pathname.startsWith("/login")) {
        setTimeout(()=>history.push("/login"), 10)
      }
    }
    checkRoute(history.location)
    const unListen = history.listen((loc) => { checkRoute(loc) })
    return () => unListen()
  }, [history])

  return <IonApp>
    <IonReactRouter history={history}>
      <IonTabs>
        <IonRouterOutlet>
          <Route path="/users/notes" component={NotesPage} exact />
          <Route path="/users/patients" component={PatientsPage} exact />
          <Route path="/users/nurses" component={NursesPage} exact />
          <Route path="/login" component={Login} />
          <Route path="/" render={() => <Redirect to="/users/notes" />} exact={true} />
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="notes" href="/users/notes">
            <IonIcon icon={triangle} />
            <IonLabel>Notes</IonLabel>
          </IonTabButton>
          <IonTabButton tab="patients" href="/users/patients">
            <IonIcon icon={ellipse} />
            <IonLabel>Patients</IonLabel>
          </IonTabButton>
          <IonTabButton tab="nurses" href="/users/nurses">
            <IonIcon icon={square} />
            <IonLabel>Nurses</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
}

export default App
