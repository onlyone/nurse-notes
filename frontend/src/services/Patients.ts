import auth from "./Authentication"
import { patientsUrl } from "./Urls"

export interface Patient {
  id: number
  name: string
}

class PatientService {
  public static all = () => {
    return fetch(`${patientsUrl}/api/v1/patients/`, {
      method: "GET",
      headers: { "Authorization": "Bearer " + auth._token },
    }).then(r => {
      if (r.ok) return r.json() as Promise<Patient[]>
      throw r
    })
  }

  public static show = (id: number) => {
    return fetch(`${patientsUrl}/api/v1/patients/show?id=` + id, {
      method: "GET",
      headers: { "Authorization": "Bearer " + auth._token },
    }).then(r => {
      if (r.ok) return r.json() as Promise<Patient>
      throw r
    })
  }
}

export default PatientService
