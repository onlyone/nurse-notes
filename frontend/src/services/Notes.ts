import auth from "./Authentication"
import { notesUrl } from "./Urls"

export interface Note {
  id?: number
  text: string
  patient_id: number
  created_at?: number
  updated_at?: number
}

class NoteService {
  public static validate = (n: Note) => n.text.trim() && n.patient_id > 0

  public static all = () => {
    return fetch(`${notesUrl}/api/v1/notes/`, {
      method: "GET",
      headers: { "Authorization": "Bearer " + auth._token },
    }).then(r => {
      if (r.ok) return r.json() as Promise<Note[]>
      throw r
    })
  }

  public static get = (id: number) => {
    return fetch(`${notesUrl}/api/v1/notes/show?id=` + id, {
      method: "GET",
      headers: { "Authorization": "Bearer " + auth._token },
    }).then(r => {
      if (r.ok) return r.json() as Promise<Note>
      throw r
    })
  }

  public static add = (note: Note) => {
    return fetch(`${notesUrl}/api/v1/notes/insert`, {
      method: "POST",
      headers: { "Authorization": "Bearer " + auth._token },
      body: JSON.stringify(note),
    }).then(r => {
      if (r.ok) return r.json() as Promise<Note>
      throw r
    })
  }

  public static update = (note: Note) => {
    console.log("updating", note)
    return fetch(`${notesUrl}/api/v1/notes/update`, {
      method: "POST",
      headers: { "Authorization": "Bearer " + auth._token },
      body: JSON.stringify(note),
    }).then(r => {
      if (r.ok) return r.json() as Promise<Note>
      throw r
    })
  }

  public static remove = (note: Note) => {
    return fetch(`${notesUrl}/api/v1/notes/delete`, {
      method: "POST",
      headers: { "Authorization": "Bearer " + auth._token },
      body: JSON.stringify(note),
    }).then(r => {
      if (r.ok) return r.json() as Promise<Note>
      throw r
    })
  }
}

export default NoteService
