import auth from "./Authentication"
import { usersUrl } from "./Urls"

export interface User {
  id: string
  username: string
}

class UserService {
  public static all = () => {
    return fetch(`${usersUrl}/api/v1/users/`, {
      method: "GET",
      headers: { "Authorization": "Bearer " + auth._token },
    }).then(r => {
      if (r.ok) return r.json() as Promise<User[]>
      throw r
    })
  }

  public static show = (id: number) => {
    return fetch(`${usersUrl}/api/v1/users/show?id=` + id, {
      method: "GET",
      headers: { "Authorization": "Bearer " + auth._token },
    }).then(r => {
      if (r.ok) return r.json() as Promise<User>
      throw r
    })
  }
}

export default UserService
