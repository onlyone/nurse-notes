const baseUrl = window.location.protocol + "//" + window.location.hostname

export const authUrl = baseUrl + ":8080"
export const usersUrl = baseUrl + ":8081"
export const patientsUrl = baseUrl + ":8082"
export const notesUrl = baseUrl + ":8083"

