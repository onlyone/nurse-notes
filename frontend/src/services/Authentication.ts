import { authUrl } from "./Urls"

export interface Credentials {
  username: string
  password: string
}

class Authentication {
  public isAuthenticated = false
  public username = ""
  public _token = ""
  private _lastErr = ""

  public retrieveToken = (cred: Credentials) => {
    return fetch(`${authUrl}/api/v1/token`, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(cred),
    }).then(r => {
      if (r.ok) return r.json()
      throw r.text()
    }).then(data => {
      this._token = data.token
      return this.retrieveInfo()
    }).catch(data => this._lastErr = data.error)
  }
  public retrieveInfo = () => {
    return fetch(`${authUrl}/api/v1/info`, {
      method: "GET",
      headers: {
        "Accept": "*/*",
        "Authorization": "Bearer " + this._token,
      },
    }).then(r => {
      if (r.ok) return r.json()
      throw r.text()
    }).then(data => {
      this.isAuthenticated = true
      this._lastErr = ""
      console.log("hello!", this.username = data.user)
    }).catch(r => {
      if (r.json) return r.json().then((data: any) => this._lastErr = data.error)
    })
  }
  public resetToken = () => {
    console.log("resetting...")
    this._token = ""
    this.username = ""
    this.isAuthenticated = false
    this._lastErr = ""
  }
  public getError = () => this._lastErr
}

const auth = new Authentication()

export default auth
