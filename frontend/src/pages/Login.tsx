import {
  IonButton,
  IonButtons,
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonContent,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonToolbar
} from "@ionic/react"
import React, { useState } from "react"
import auth, { Credentials } from "../services/Authentication"
import './Login.css';
import { useHistory } from "react-router"

const devCred = {username: "jack", password: "123456789"}
const Login: React.FC = () => {
  const history = useHistory()
  const [cred, setCred] = useState<Credentials>({ username: "", password: "" })
  const [err, setErr] = useState("")

  const updateUsername = (e: any) => setCred({ ...cred, username: e.detail.value! })
  const updatePassword = (e: any) => setCred({ ...cred, password: e.detail.value! })
  const handleLogin = (cred: Credentials) => auth.retrieveToken(cred).then(() => {
    setErr(auth.getError())
    if (!auth.getError()) history.push("/users/notes")
  })

  return <IonPage>
    <IonContent>
      <IonCard className="login-form">
        <IonCardHeader>
          <IonCardTitle>Login</IonCardTitle>
          <IonCardSubtitle>Please Input Your Credentials {err ? "- " + err : ""}</IonCardSubtitle>
        </IonCardHeader>
        <IonItem>
          <IonLabel>Username</IonLabel>
          <IonInput value={cred.username} onIonChange={updateUsername} />
        </IonItem>
        <IonItem>
          <IonLabel>Password</IonLabel>
          <IonInput type="password" value={cred.password} onIonChange={updatePassword} />
        </IonItem>
        <IonToolbar>
          <IonButtons slot="start">
            <IonButton color="warning" onClick={() => handleLogin(devCred)}>just log me in (dev only mode)</IonButton>
          </IonButtons>
          <IonButtons slot="end">
            <IonButton onClick={() => handleLogin(cred)}>login</IonButton>
          </IonButtons>
        </IonToolbar>
      </IonCard>
    </IonContent>
  </IonPage>
}
export default Login
