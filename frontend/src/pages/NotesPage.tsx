import React from 'react'
import {
  IonButton,
  IonButtons,
  IonCard, IonCardContent,
  IonContent,
  IonHeader, IonInput, IonItem, IonLabel,
  IonList,
  IonPage, IonSelect, IonSelectOption,
  IonTitle,
  IonToolbar
} from "@ionic/react"
import './NotesPage.css'
import LogoutButton from "../components/LogoutButton"
import {useHistory} from "react-router"
import NoteService, {Note} from "../services/Notes"
import PatientService, {Patient} from "../services/Patients"
import auth from "../services/Authentication"

const NotesPage: React.FC = () => {
  const history = useHistory()
  const [lastNurse, setLastNurse] = React.useState<string>()
  const [notes, setNotes] = React.useState<Note[]>()
  const [patients, setPatients] = React.useState<Patient[]>()
  const [editorText, setEditorText] = React.useState("")
  const [editorPatientId, setEditorPatientId] = React.useState(0)
  const [editorNoteId, setEditorNoteId] = React.useState(0)

  if (history.location.pathname.startsWith("/users/notes")) {
    if (auth.username !== lastNurse) {
      setLastNurse(auth.username)
      resetEditor()
    } else if (!notes) updateNoteList()
    if (!patients) PatientService.all().then(setPatients)
  }

  function resetEditor() {
    setEditorText("")
    setEditorPatientId(0)
    setEditorNoteId(0)
    setNotes(() => {
      updateNoteList()
      return []
    })
  }

  function updateNoteList() {
    NoteService.all().then(notes => {
      setNotes(notes)
      console.log(notes)
    })
  }

  const handleAddNote = () => {
    const noteData = { text: editorText, patient_id: editorPatientId }
    if (NoteService.validate(noteData)){
      NoteService.add(noteData).then(() => updateNoteList())
      setEditorNoteId(0)
    }
  }

  const handleEditNote = (note: Note) => {
    setEditorText(note.text)
    setEditorPatientId(note.patient_id)
    setEditorNoteId(note.id!)
  }

  const handleUpdateNote = () => {
    const noteData = { id: editorNoteId, text: editorText, patient_id: editorPatientId }
    if (NoteService.validate(noteData)) {
      NoteService.update(noteData).then(() => updateNoteList())
      setEditorNoteId(0)
    }
  }

  const handleRemoveNote = (note: Note) => {
    if (editorNoteId === note.id) setEditorNoteId(0)
    NoteService.remove(note).then(() => updateNoteList())
  }

  const findPatientName = (id: number) => {
    if (!patients?.length) return id
    const p = patients.find(p => p.id === id)
    return p ? p.name : id
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Notes</IonTitle>
          <IonButtons slot="end">
            <LogoutButton/>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Notes</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonCard>
          <IonItem>
            <IonLabel>Patient:</IonLabel>
            <IonSelect value={editorPatientId} placeholder="Select A Patient for you note"
                       onIonChange={e => setEditorPatientId(e.detail.value)}>
              {patients && patients.map(p => <IonSelectOption key={p.id} value={p.id}>{p.name}</IonSelectOption>)}
            </IonSelect>
          </IonItem>
          <IonCardContent>
            <IonInput value={editorText} placeholder="Please take some notes" type="text"
                      onIonInput={(e: any) => setEditorText(e.target.value)} />
            <IonButton disabled={!NoteService.validate({ text: editorText, patient_id: editorPatientId })}
                       onClick={handleAddNote}>Add</IonButton>
            {editorNoteId ? <IonButton color="warning"
                disabled={!NoteService.validate({ text: editorText, patient_id: editorPatientId })}
                onClick={handleUpdateNote}>Update</IonButton>
              : ""}
          </IonCardContent>
        </IonCard>
        <IonList>
          {notes && notes.map(note =>
            <IonItem key={note.id} color={note.id === editorNoteId ? "warning" : ""}>
              <IonLabel>
                <h3>Patient {findPatientName(note.patient_id)}</h3>
                <p>{note.text}</p>
                <small>Created: {note.created_at}, Last modified: {note.updated_at}</small>
              </IonLabel>
              <IonButtons slot="end">
                <IonButton onClick={() => handleEditNote(note)}>edit</IonButton>
                <IonButton onClick={() => handleRemoveNote(note)}>X</IonButton>
              </IonButtons>
            </IonItem>
          )}
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default NotesPage
