import React from 'react';
import {
  IonButton,
  IonButtons,
  IonCard,
  IonContent,
  IonHeader, IonItem,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar
} from "@ionic/react"
import './PatientsPage.css';
import LogoutButton from "../components/LogoutButton"
import {useHistory} from "react-router"
import PatientService, {Patient} from "../services/Patients"

const PatientsPage: React.FC = () => {
  const history = useHistory()
  const [patients, setPatients] = React.useState<Patient[]>()
  if (history.location.pathname.startsWith("/users/patients")) {
    if (!patients)
      PatientService.all().then(patients => {
        setPatients(patients)
        console.log(patients)
      })
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Patients</IonTitle>
          <IonButtons slot="end">
            <LogoutButton />
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Patients</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonCard>
          <IonButton disabled>Add</IonButton>
        </IonCard>
        <IonList>
          {patients && patients.map(u =>
            <IonItem key={u.id}>
              {u.name}
              <IonButtons slot="end">
                <IonButton disabled>X</IonButton>
              </IonButtons>
            </IonItem>
          )}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default PatientsPage;
