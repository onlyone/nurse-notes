import React from 'react';
import {
  IonButton,
  IonButtons,
  IonCard,
  IonContent,
  IonHeader,
  IonItem,
  IonList,
  IonPage,
  IonTitle,
  IonToolbar
} from "@ionic/react"
import './NursesPage.css';
import LogoutButton from "../components/LogoutButton"
import UserService, {User} from "../services/Users"
import {useHistory} from "react-router"

const NursesPage: React.FC = () => {
  const history = useHistory()
  const [users, setUsers] = React.useState<User[]>()
  if (history.location.pathname.startsWith("/users/nurses")) {
    if (!users)
      UserService.all().then(users => {
        setUsers(users)
        console.log(users)
      })
  }
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Users</IonTitle>
          <IonButtons slot="end">
            <LogoutButton />
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Nurses</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonCard>
          <IonButton disabled>Add</IonButton>
        </IonCard>
        <IonList>
          {users && users.map(u =>
            <IonItem key={u.id}>
              {u.username}
              <IonButtons slot="end">
                <IonButton disabled>X</IonButton>
              </IonButtons>
            </IonItem>
          )}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default NursesPage;
