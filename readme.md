# Basic Architecture Concept
- Client and Server with API(RPC)
- Server are composed of microservices only
- Microservices are communicated using the same API for Client
- Most API routes are guarded by JWT

# Steps for Running the Dev System the First time
- Add a `.app-key` file under path `./services/authentication` with a 10 bytes long (or longer) key
- `docker-compose up`
- navigate to `localhost:8100`
- login using either the convenient `DEV ONLY` login button, or use these credentials:
```
{"jack", "123456789"},
{"mary", "33yu1234"},
```

# API Routes

## Authentication Service
- `/api/v1/token` 
retrieve a jwt token using username and password   
- `/api/v1/info` 
confirm and send user info if token is valid

## Note Service
- `/api/v1/notes/` 
- `/api/v1/notes/show` 
- `/api/v1/notes/insert` 
- `/api/v1/notes/update` 
- `/api/v1/notes/delete` 

## Patient Service
- `/api/v1/patients/` 
- `/api/v1/patients/show` 
- `/api/v1/patients/insert` 
- `/api/v1/patients/update` 
- `/api/v1/patients/delete` 

## User Service
- `/api/v1/users/` 
- `/api/v1/users/show` 
- `/api/v1/users/insert` 
- `/api/v1/users/update` 
- `/api/v1/users/delete` 
    
# Naming Convention
- Go source coding follows the standard Go convention according to go tool
- Files will be kebab case

# Comments
I used a minimum amount of annotations not only due to time constraint, but also I think code should be readable 
by itself. If there is anything unclear, it is more efficient to use the time to improve the code instead.
However, some key comments do need to be added or improved too, such as file and package level comments.

# Tests
- There is a simple test for authentication only for demonstration purpose

# Problems
## Missing tests
- Problem: some necessary unit tests and functional tests are missing due to time constraint
- Solution: add them back in the future as early as possible to maintain development speed

## Too much information from backend
- Problem: some errors are sent from backend though API revealing potentially too much information
- Solution: use predefined generic messages instead of reporting err.Error()

## Communication between microservices
- Problem: microservices are communicating using web APIs which less efficient and/or robust
- Solution: switch to gPRC or solutions dedicated for inter microservice communication

## Use of database id in APIs
- Problem: database id is sequential and can be attacked with heuristic methods
- Solution: introduce a more randomized identifier for API queries such as UUID in the future
